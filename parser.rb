# TODO 
# get information from 
# https://www.petsonic.com/snacks-huesos-para-perros/
# GET ALL PRODUCTS FROM CATEGORY
# full name -? [0-9 gr]/ price / image

# EXAMPLE: Galletas Granja para Perro - 200 gr /
# 1.35 / http://www.petsonic.com/5830- 
# large_default/galletas-granja-para-perro.jpg

# To download - curb
# Parsing html - nokogirl
# CSV - csv
require 'csv'
require 'curb'
require 'nokogiri'
link2 = 'https://www.petsonic.com/purina-pro-plan-delibakie-biscotti-para-perros.html#/1267-peso-90_gr'


def get_details(link)
    html = return_html(link)
    html_doc = Nokogiri::HTML(html)
    img_url = html_doc.xpath('//*[@id="view_full_size"]//@src')
    puts "Found image of this product... "
    name_of_product = html_doc.xpath('//h1[@class="product_main_name"]//text()')
    puts "Found name of this product... "
    attributes = html_doc.xpath('//*[@class="attribute_list"]//li')
    attributes.each do |attrib|
        weights = attrib.xpath('//span[@class="radio_label"]//text()')
        prices = attrib.xpath('//span[@class="price_comb"]//text()')
        puts "Found prices of this product..."
        #puts "Done"
        if prices.length != 0 and weights.to_a.length != 0
            return name_of_product.text, img_url.text, weights.to_a, prices
        end
    end

end


def return_html(link)
    # Returns html text of the given link
    puts "Retrieving html from " + link.to_s
    return Curl.get(link).body_str
end

def parser(link)
    
    item_list = []
    c = 1 
    html = return_html(link)
    html_doc = Nokogiri::HTML(html)
    while html_doc.xpath('//*[@class="loadMore next button lnk_view btn btn-default"]').to_a.length !=0 do
        center_column = html_doc.xpath('//div[@id="center_column"]/ul[@id="product_list"]//div[@class="pro_outer_box"]//a[@class="product-name"]/@href')
        for elem in center_column
            sleep(rand(5))
            name, img, weights, prices = get_details(elem)
            if weights.to_s != ''
                for i in 0..weights.length-1
                    l = []
                    full_name = name.strip + ' - ' + weights[i].to_s.strip
                    price = prices[i].to_s.split(' ')[0].strip
                    l << full_name
                    l << price
                    l << img.to_s.strip
                    if not item_list.include? l
                        item_list.push(l)
                        write_csv('./test.csv', l)
                    end
                end
            end
        end
        c=c+1
        html = return_html(link+'?p='+c.to_s)
        html_doc = Nokogiri::HTML(html)
    end
    puts "Page is parsed"
    sleep(rand(5..10))
    
end

def get_categories(html)
    categories = []
    html_doc = Nokogiri::HTML(html)
    links = html_doc.xpath('//*[@id="subcategories"]//h5//@href')
    for cat in links 
        categories << cat.text
    end
    puts "Found categories ..."
    return categories
end

def write_csv(path, info)
    CSV.open(path, "a") do |csv|
        puts "Writing on " + path
        csv << info

    end
end



#categories =  get_categories(return_html('https://www.petsonic.com/snacks-huesos-para-perros/'))
#for category in categories
    #parser(category)
#end

link = 'https://www.petsonic.com/snacks-huesos-para-perros/'
parser(link)
